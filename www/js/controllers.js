angular.module('starter.controllers', ['ionic.utils'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	//$scope.$on('$ionicView.enter', function(e) {
	//});

	// Form data for the login modal
	$scope.loginData = {};

	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/login.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Triggered in the login modal to close it
	$scope.closeLogin = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.login = function() {
		$scope.modal.show();
	};

	// Perform the login action when the user submits the login form
	$scope.doLogin = function() {
		console.log('Doing login', $scope.loginData);

		// Simulate a login delay. Remove this and replace with your login
		// code if using a login system
		$timeout(function() {
		  $scope.closeLogin();
		}, 1000);
	};
})

.controller('HomeCtrl', function($scope, $localstorage, $ionicModal) {

	// refactor!
	$scope.words = [];
	
	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/add-word.html', {
		scope: $scope
	}).then(function(addWordModal) {
		$scope.addWordModal = addWordModal;
	});

	// Triggered in the login addWordModal to close it
	$scope.closeWordModal = function() {
		$scope.addWordModal.hide();
	};

	// Open the login addWordModal
	$scope.openWordModal = function() {
		$scope.addWordModal.show();
	};

	$scope.addWord = function() {
		// delete $$localstorage & scope.words & ionic utils
		$scope.words.push({base: $scope.addWord.baseLanguage, foreign: $scope.addWord.foreignLanguage});
		console.log($scope.words);

		$scope.closeWordModal();
	}
})

.controller('AllWordsCtrl', function($scope) {
	$scope.words = [
		{ base: 'hello', foreign: 'hola' },
		// { base: 'porridge', foreign: 'gachas de avena' },
		{ base: 'giraffe', foreign: 'jirafa' },
		{ base: 'cauliflower', foreign: 'coliflor' },
		{ base: 'fool', foreign: 'bobo' },
		{ base: 'fox', foreign: 'zorro' },
		{ base: 'honey', foreign: 'miel' },
		{ base: 'girl', foreign: 'muchacha' },
		{ base: 'beer', foreign: 'cerveza' },
		{ base: 'good', foreign: 'bien' },
	];

	$scope.deleteWord = function(index) {
		console.log("delete index: " + index);
		$scope.words.splice(index, 1);
	}
})

.controller('TestCtrl', function($scope, $ionicLoading, $timeout, $window) {

	$timeout(function() {
		document.getElementById('test-input').focus();
	}, 500);

	$scope.words = [
		{ base: 'hello', foreign: 'hola' },
		// { base: 'porridge', foreign: 'gachas de avena' },
		{ base: 'giraffe', foreign: 'jirafa' },
		{ base: 'cauliflower', foreign: 'coliflor' },
		{ base: 'fool', foreign: 'bobo' },
		{ base: 'fox', foreign: 'zorro' },
		{ base: 'honey', foreign: 'miel' },
		{ base: 'girl', foreign: 'muchacha' },
		{ base: 'beer', foreign: 'cerveza' },
		{ base: 'good', foreign: 'bien' },
	];

	$scope.currentWord = $scope.words[(Math.floor((Math.random() * $scope.words.length)))];
	$scope.nextWord = $scope.words[(Math.floor((Math.random() * $scope.words.length)))];

	$scope.errorWord = false;
	$scope.showAnswer = false;

	$scope.checkWord = function() {
		if ($scope.currentWord.foreign == $scope.checkWord.translation) {
			var time = 1000;
			if ($scope.errorWord) {
				time = 2500;
			}
			$scope.errorWord = false;
			$scope.showAnswer = false;

			animateTimeBar(time);
			// $timeout(function() {
			// 	changeWord();
			// }, time);
		} else {
			$scope.errorWord = true;
			$scope.showAnswer = true;
			$timeout(function() {
				document.getElementById('test-input').focus();
			}, 300);
		}
	}

	var animateTimeBar = function(time) {
		var bar = document.getElementById('time-bar');
		var myElement = angular.element(bar);

		var animation = collide.animation({ 
			duration: time,
			percent: 0,
			reverse: false
		})
		.on('step', function(v) {
			var width = 100;
			// myElement.css('webkitTransform', 'translate3d(' + (v * (width)) +'%, 0, 0)');
			myElement.css('width', (v * (width)) + '%');
		})
		.on('stop', function() {
		})
		.on('complete', function(){
			$timeout(function() {
				changeWord();
			}, 100);
		})
		.start();
	}

	var changeWord = function() {
		
		$scope.nextWord = $scope.words[(Math.floor((Math.random() * $scope.words.length)))];

		var word = document.querySelector(".current-word");
		var word2 = document.querySelector(".next-word");
		var myElement = angular.element(word);
		var myElement2 = angular.element(word2);
		var offset = myElement.prop('offsetTop');
		myElement2.css('top', offset+'px');

		var wordConatiner = document.querySelector(".word-container");
		var contElem = angular.element(wordConatiner);

		var animation = collide.animation({
			easing: 'ease-out', 
			duration: 250,
			percent: 0,
			reverse: false
		})
		.on('step', function(v) {
			var width = 100;
			myElement.css('webkitTransform', 'translate3d(-' + (v * (width)) +'%, 0, 0)');
			myElement2.css('webkitTransform', 'translate3d(-' + (v * (width)) +'%, 0, 0)');
		})
		.on('stop', function() {
		})
		.on('complete', function(){
			$scope.$apply(function () {
				$scope.currentWord = $scope.nextWord;

				$scope.checkWord.translation = '';
				document.getElementById('test-input').focus();
			});
		})
		.start();
	}

})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
